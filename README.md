# General information

*External contributions are very welcome!*

- [Course website](https://homes.di.unimi.it/bellettini/sito/vc.html)
- [Course material repo](https://gitlab.com/verificaEconvalida/materiale2020)
- Lessons schedule: Tuesday and Thursday 10:30 AM

# Lesson 1

*Thu 12 Mar 2020*

- [Lesson video](https://www.youtube.com/watch?v=r7ELhoDcJRk)
- [Sli.do](https://app.sli.do/event/ierakoj5/) for questions, polls and live quizzes
- Useful links:
  - [XUnit testing patterns](http://xunitpatterns.com)
    - By next lesson, read:
      - [Goals of Test Automation](http://xunitpatterns.com/Goals%20of%20Test%20Automation.html)
      - [Principles of Test Automation](http://xunitpatterns.com/Principles%20of%20Test%20Automation.html)
      - [Test Smells](http://xunitpatterns.com/Test%20Smells.html)
  - [checkJU repo](https://gitlab.com/verificaEconvalida/checkJU)
    - To do: clone, run `gradle test`

### First assignment

Implement and test a `Triangle` class of which we have the interface and specification:

```java
package it.unimi.di.vec.ass1;

public interface Triangle {
    // the constructor reads 3 integers from standard input interpreting them
    // as the lengths of the sides of the triangle

    // write in the standard output the type of triangle:
    // equilateral, isosceles, scalene
    void describe();
}
```

Fork [this repository](https://gitlab.com/verificaEconvalida/triangles) and share it *at least* with `@carlobellettini` and `@mmonga`.

Try to complete it at least by Sunday night.

While implementing it, keep in mind the readings from *XUnit testing patterns* listed above.
